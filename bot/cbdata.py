from aiogram.filters.callback_data import CallbackData

class NewGameFactory(CallbackData, prefix="newgame"):
    size: int
    bombs: int
    asSeparate: bool


class ClickFactory(CallbackData, prefix='click'):
    game_id: str
    x: int
    y: int


class ChangeFlagFactory(CallbackData, prefix="flag"):
    game_id: str
    action: str
    x: int
    y: int


class ChangeModeFactory(CallbackData, prefix='chmod'):
    game_id: str
    mode: int