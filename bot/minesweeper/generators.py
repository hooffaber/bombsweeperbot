import random
from typing import List, Tuple


def print_fiend(field: List[List]):
    for i in range(len(field)):
        print(field[i])


def generate_field(k: int) -> List[List]:
    result = []
    for i in range(k):
        result.append([0]*k)
    return result


def generate_valid_neighbours(x: int, y: int, size: int) -> set[Tuple]:
    if x < 0 or x >= size or y < 0 or y >= size:
        raise RuntimeError(f"Bad coords ({x},{y}) in generate neigbours function")
    
    dirs = [(1, 0), 
            (-1, 0), 
            (0, 1), 
            (0, -1), 
            (1, 1), 
            (-1, -1), 
            (1, -1), 
            (-1, 1)]
    result = set()

    for dir in dirs:
        dx, dy = dir
        if (x + dx) >= 0 and (x + dx) < size \
            and (y + dy) >= 0 and (y + dy) < size \
                and (x + dx, y + dy) not in result:
            result.add((x + dx, y + dy))
    
    return result


def generate_game(size: int, max_bombs: int) -> List[List]:
    field = generate_field(size)

    bomb_cnt = 0

    while bomb_cnt < max_bombs:
        x, y = random.randint(0, size - 1), random.randint(0, size - 1)

        if field[x][y] == '*':
            continue
        field[x][y] = '*'
        neigs = generate_valid_neighbours(x, y, size)
        for neig in neigs:
            neig_x, neig_y = neig
            if field[neig_x][neig_y] != '*':
                field[neig_x][neig_y] += 1
        
        bomb_cnt += 1
    return field

if __name__ == '__main__':
    generate_game(4, 5)