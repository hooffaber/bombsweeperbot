class FieldSquareStatus:
    HIDDEN = 0
    OPEN = 1
    BOMB = 2
    FLAG = 3

class ClickMode:
    OPEN = 0
    FLAG = 1