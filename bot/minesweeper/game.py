from typing import List
from uuid import uuid4

from minesweeper.generators import generate_field, generate_game
from minesweeper.states import ClickMode

def get_newgame_data(size: int, bombs: int):

    minefield: List[List] = generate_game(size, bombs)
    maskfield: List[List] = generate_field(size)

    return minefield, maskfield