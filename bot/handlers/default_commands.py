from aiogram import Router
from aiogram.fsm.context import FSMContext
from aiogram.types import Message, InlineKeyboardButton
from aiogram.utils.keyboard import InlineKeyboardBuilder
from aiogram.filters import Command

from keyboards.newgamekb import get_start_kb
from cbdata import NewGameFactory
default_cmd_router = Router()


@default_cmd_router.message(Command("start"))
async def cmd_start(message: Message, state: FSMContext):
    await message.answer("""
        Press to start <b>NEW GAME</b>.\nPrevious one will be dismissed.\nThe field would be a <b>5x5</b> one.""", 
        reply_markup=get_start_kb()
        )


@default_cmd_router.message(Command("help"))
async def cmd_help(message: Message):
    await message.answer("Обработка /help")

