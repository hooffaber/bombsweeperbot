from typing import List
from uuid import uuid4

from aiogram import Router
from aiogram.fsm.context import FSMContext
from cbdata import NewGameFactory, ClickFactory, ChangeModeFactory, ChangeFlagFactory
from aiogram.types import CallbackQuery

from minesweeper.states import ClickMode, FieldSquareStatus
from keyboards.kb_minefield import make_kb_minefield
from minesweeper.generators import generate_game, generate_field
from minesweeper.game import get_newgame_data

callbackRouter = Router()


def count_free_squares(fieldmask: List[List]) -> int:
    if not len(fieldmask):
        raise ValueError("Bad fieldmask in there.")
    
    cntr = 0
    for i in range(len(fieldmask)):
        for j in range(len(fieldmask[0])):
            if not fieldmask[i][j] or fieldmask[i][j] == FieldSquareStatus.BOMB:
                cntr += 1
    
    return cntr


def all_flags_match(minefield: List[List[str | int]], maskfield: List[List[int]]) -> bool:
    size = len(minefield)
    for x in range(size):
        for y in range(size):
            if maskfield[x][y] == FieldSquareStatus.FLAG and minefield[x][y] != "*":
                return False
    return True


@callbackRouter.callback_query(NewGameFactory.filter())
async def newgame_callback(callback: CallbackQuery, state: FSMContext, callback_data: NewGameFactory):

    size = callback_data.size
    bombs = callback_data.bombs
    asSeparte = callback_data.asSeparate
    game_id = str(uuid4())

    minefield, maskfield = get_newgame_data(size=size, bombs=bombs)
    curr_mode = ClickMode.OPEN

    game_dict = {
        "game_id": game_id,
        "size": size,
        "bombs_count": bombs,
        "minefield": minefield,
        "maskfield": maskfield,
        "click_mode": curr_mode
    }

    await state.set_data(game_dict)
    await callback.message.edit_text(f"Game started.{game_dict}")

    await callback.message.answer("Debug message.",
                                  reply_markup=make_kb_minefield(minefield=minefield, 
                                                                 fieldmask=maskfield, 
                                                                 game_id=game_id, 
                                                                 click_mode=curr_mode))

    await callback.answer()


@callbackRouter.callback_query(ClickFactory.filter())
async def field_click_call(callback: CallbackQuery, state: FSMContext, callback_data: ClickFactory):
    game_id = callback_data.game_id

    x = callback_data.x
    y = callback_data.y

    game_dict: dict = await state.get_data()

    if game_id != game_dict['game_id']:
        await callback.answer(show_alert=True, text="This game is inaccessible, because there is more recent one!")
        return

    minefield, maskfield, click_mode = game_dict['minefield'], game_dict['maskfield'], game_dict['click_mode']

    if minefield[x][y] == "*":
        await callback.message.edit_text(text='You have lost.', reply_markup=None)
        return

    if minefield[x][y] != "*":
        maskfield[x][y] = FieldSquareStatus.OPEN
        
        game_dict.update({'maskfield': maskfield})
    

    if not count_free_squares(maskfield):
        if all_flags_match(minefield=minefield, maskfield=maskfield):
            await callback.message.edit_text(text='You have won!', reply_markup=None)
            return
        else:
            await callback.answer(
                    show_alert=True,
                    text="Looks like you've placed more flags than there are bombs on field. Please check them again."
                )        
             
    
    await callback.message.edit_reply_markup(reply_markup=make_kb_minefield(minefield=minefield, 
                                                                    fieldmask=maskfield, 
                                                                    game_id=game_id, 
                                                                    click_mode=click_mode))

    await callback.answer()


@callbackRouter.callback_query(ChangeModeFactory.filter())
async def change_flag_click(callback: CallbackQuery, state: FSMContext, callback_data: ChangeModeFactory):
    game_dict = await state.get_data()

    game_id, mode = callback_data.game_id, callback_data.mode

    # if game_id != game_dict['game_id']:
    #     await callback.answer(show_alert=True, text="This game is inaccessible, because there is more recent one!")
    #     return 

    await state.update_data({"click_mode": mode})

        
             

    await callback.message.edit_text(callback.message.text, reply_markup=make_kb_minefield(minefield=game_dict['minefield'], 
                                                                    fieldmask=game_dict['maskfield'], 
                                                                    game_id=game_id, 
                                                                    click_mode=mode))
    

@callbackRouter.callback_query(ChangeFlagFactory.filter())
async def change_flag_call(callback: CallbackQuery, state: FSMContext, callback_data: ChangeFlagFactory):
    game_dict = await state.get_data()
    game_id = callback_data.game_id

    if game_id != game_dict['game_id']:
        await callback.answer(show_alert=True, text="This game is inaccessible, because there is more recent one!")
        return

    x, y = callback_data.x, callback_data.y

    action = callback_data.action

    fieldmask = game_dict['maskfield']
    mask = fieldmask[x][y]
    if action == "add":
        fieldmask[x][y] = FieldSquareStatus.FLAG
    elif action == 'remove':
        fieldmask[x][y] = FieldSquareStatus.HIDDEN
    
    await state.update_data({"maskfield": fieldmask})

    print(count_free_squares(fieldmask))

    if not count_free_squares(fieldmask):
        if all_flags_match(minefield=game_dict["minefield"], maskfield=fieldmask):
            await callback.message.edit_text(text='You have won!', reply_markup=None)
            return
        else:
            await callback.answer(
                    show_alert=True,
                    text="Looks like you've placed more flags than there are bombs on field. Please check them again."
                )    
         

    await callback.message.edit_text(callback.message.text, reply_markup=make_kb_minefield(minefield=game_dict['minefield'], 
                                                                    fieldmask=fieldmask, 
                                                                    game_id=callback_data.game_id, 
                                                                    click_mode=game_dict['click_mode']))
         