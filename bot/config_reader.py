import os
import configparser

from dataclasses import dataclass

DEFAULT_CONFIG_DIR, DEFAULT_CONFIG_NAME = 'config', 'bot.ini'

@dataclass
class TgBot:
    bot_token: str

@dataclass
class Config:
    bot: TgBot


def get_default_path():

    current_dir = os.path.dirname(os.path.abspath(__file__))
    def_config_path = os.path.join(current_dir, DEFAULT_CONFIG_DIR, DEFAULT_CONFIG_NAME)

    if os.path.exists(def_config_path):
        return def_config_path
    else:
        raise RuntimeError("No default config file at /bot/config/")


def load_config(path: str  = get_default_path()):
    config = configparser.ConfigParser()
    config.read(path)

    bot_cfg = config['bot']

    return Config(bot=TgBot(bot_token=bot_cfg['BOT_TOKEN']))

