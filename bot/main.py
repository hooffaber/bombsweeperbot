import os
import logging
import asyncio
from aiogram import Dispatcher, Bot
from aiogram.fsm.storage.memory import MemoryStorage
from config_reader import load_config
from handlers.default_commands import default_cmd_router
from handlers.callbacks import callbackRouter
from commands import set_commands

async def main():

    logging.basicConfig(
        level=logging.INFO,
        format="%(asctime)s - %(levelname)s - %(name)s - %(message)s",
    )

    current_dir = os.path.dirname(os.path.abspath(__file__))

    config = load_config()

    bot = Bot(token=config.bot.bot_token, parse_mode='HTML')
    await set_commands(bot)
    dp = Dispatcher(storage=MemoryStorage())
    dp.include_routers(default_cmd_router,
                       callbackRouter)

    await dp.start_polling(bot)    


asyncio.run(main())