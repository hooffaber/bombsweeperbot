from typing import List
from aiogram.utils.keyboard import InlineKeyboardBuilder
from minesweeper.states import FieldSquareStatus, ClickMode
from cbdata import ClickFactory, ChangeFlagFactory, ChangeModeFactory

def make_kb_minefield(minefield: List[List], 
                      fieldmask: List[List], 
                      game_id: str, click_mode: int):
    
    size = len(minefield)
    builder = InlineKeyboardBuilder()
    
    for x in range(len(minefield)):
        for y in range(len(minefield[0])):
            if fieldmask[x][y] == FieldSquareStatus.HIDDEN:
                if click_mode == ClickMode.OPEN:
                    builder.button(text='•', 
                                   callback_data=ClickFactory(game_id=game_id, x=x, y=y))
                else:
                    builder.button(text='•', 
                                   callback_data=ChangeFlagFactory(game_id=game_id, action='add', x=x, y=y))
            elif fieldmask[x][y] == FieldSquareStatus.BOMB:
                builder.button(text='💥', callback_data='ignore')
            elif fieldmask[x][y] == FieldSquareStatus.FLAG:
                builder.button(text='FLAG', callback_data=ChangeFlagFactory(game_id=game_id, action='remove', x=x, y=y)) 
            elif fieldmask[x][y] == FieldSquareStatus.OPEN:
                builder.button(text=str(minefield[x][y]), callback_data='ignore')
    
    if click_mode == ClickMode.OPEN:
        text_tail = 'CLICK'
        call_data = ChangeModeFactory(game_id=game_id, mode=ClickMode.FLAG)
    else:
        text_tail = 'FLAG 🏁'
        call_data = ChangeModeFactory(game_id=game_id, mode=ClickMode.OPEN)

    builder.button(text=f'🔄 Current mode: {text_tail}', callback_data=call_data)

    builder.adjust(size)

    return builder.as_markup()