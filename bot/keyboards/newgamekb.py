from aiogram.utils.keyboard import InlineKeyboardBuilder
from aiogram.types import InlineKeyboardButton
from cbdata import NewGameFactory

def get_start_kb():
    builder = InlineKeyboardBuilder()
    builder.button(
        text='5x5, 5 bombs', callback_data=NewGameFactory(size=5, bombs=5, asSeparate=False)
    )
    builder.button(
        text='5x5, 5 bombs', callback_data=NewGameFactory(size=5, bombs=5, asSeparate=False)
    )
    builder.button(
        text='5x5, 5 bombs', callback_data=NewGameFactory(size=5, bombs=5, asSeparate=False)
    )
    builder.adjust(3)

    return builder.as_markup()