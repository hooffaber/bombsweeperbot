from aiogram import Bot
from aiogram.types import BotCommand, BotCommandScopeAllPrivateChats

commands = [
    BotCommand(command='start', description='New game'),
    BotCommand(command='help', description='How to play'),
    BotCommand(command='stats', description="Personal stats"),
]


async def set_commands(bot: Bot):
    await bot.set_my_commands(commands)
